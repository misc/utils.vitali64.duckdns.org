#!/bin/sh
#	learn-plain-fases, a simple website documentation generator
#	Copyright 2021,2022 Ferass El Hafidi <vitali64pmemail@protonmail.com>
#	This program is free software: you can redistribute it and/or modify
#	it under the terms of the GNU General Public License as published by
#	the Free Software Foundation, either version 3 of the License, or
#	(at your option) any later version.
#	Please see the LICENSE file for more details.
usage() {
	echo "Usage: ./learn.sh [OPTIONS]"
	echo "-h   : prints this help message"
	echo "-c   : remove generated website"
	echo "-l   : prints the license"
}
if [ "$1" = "-h" ]; then
	usage
	exit 0
elif [ "$1" = "-c" ]; then
	echo "Removing generated website ..."
	rm -rf www/*
	exit 0
elif [ "$1" = "-l" ]; then
	cat COPYING.GPLv3
	exit 0
elif [ "$1" = "" ]; then
	echo "Generating website ..."
else
	echo "/!\\ Invalid option."
	usage
	exit 1
fi
# Insert header
header() {
	sed "s/{this_page}$1/" "templates/header.html"
}
# Insert footer
footer() {
	# There's a trailing slash - example: /CHAT
	sed "s/{this_page}$1/" "templates/footer.html"
}
docs="$(cd docs/ && find ./*.md|sed -e 's/\.md//' -e 's/.//')"
list() {
	printf "<td class=nav>\n"
	printf " <ul class=list>\n"
	name="$(head -n 1 docs/INDEX.md)"
	printf "  <li><a href=\"/INDEX.html\">${name}</a></li>\n"
	printf "  <li><a>Get the source code</a></li>\n"
	printf "  <ul><li><a href=\"https://git.vitali64.duckdns.org/utils/fases.git\">fases.git</a></li>\n"
	printf "    <li><a href=\"https://git.vitali64.duckdns.org/misc/fases-linux.git\">fases-linux.git</a></li>\n"
	printf "    <li><a href=\"https://git.vitali64.duckdns.org/misc/utils.vitali64.duckdns.org.git\">utils.vitali64.duckdns.org.git</a></li></ul>\n"
	for filelink in ${docs}
	do
		if [ ! "${filelink}" = "/INDEX" ]; then
			name="$(head -n 1 docs${filelink}.md)"
			printf "  <li><a href=\"${filelink}.html\">${name}</a></li>\n"
		fi
	done
	printf " </ul>\n"
	printf "</td>\n"
}
for file in ${docs}
do
	header ${file} > "www/${file}.html"
	list >> "www/${file}.html"
	#sed -e '1d' "docs/${file}.md" \
	#	-e 's,!\[\([^]]*\)\](\([^)]*\)),<img src="\2" alt="\1"/>,g' \
	#	-e 's@\*\*\*.*.\*\*\*@<strong><i>&</i></strong>@g' \
	#	-e 's@\*\*.*.\*\*@<strong>&</strong>@g' \
	#	-e 's,\*\([^*<>][^*<>]*\)\*,<i>\1</i>,g' \
	#	-e 's,\[\([^]]*\)\](\([^)]*\)),<a href="\2">\1</a>,g' >> "www/${file}.html"
	echo "<td class=\"doc\">" >> "www/${file}.html"
	sed '1d' "docs/${file}.md" | markdown | sed -e 's_^<h\([123]\)>\(.*\)</h\1>_<h\1 id="\2">\2 <small><a aria-hidden="true" href="#\2">[link]</a></small></h\1>_' >> "www/${file}.html"
	echo "</td>" >> "www/${file}.html"
	footer "${file}" >> "www/${file}.html"
done
