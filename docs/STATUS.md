Status

31 utilities are currently implemented (most of them aren't done yet):

* `basename`
* `cat`
* `chmod`
* `chown`
* `cmp`
* `date`
* `dirname`
* `echo`
* `ed`
* `false`
* `head`
* `link`
* `ln`
* `ls`
* `mkdir`
* `more`
* `mv`
* `printf`
* `rm`
* `sleep`
* `tail`
* `tee`
* `test` / `[`
* `touch`
* `true`
* `tty`
* `uname`
* `unlink`
* `vi`
* `wc`

Other utilities are still in the making. Help is greatly appreciated.
