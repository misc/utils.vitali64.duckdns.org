Help us

Want to help the `fases` project? If yes, this is what you can do:

Code
----
So far, contributing code is the best way to help the project. By doing so, 
you're helping us implementing more and more utilities, and you're also 
helping us fix bugs reported by others.

Testing utilities and reporting bugs
------------------------------------
By testing utilties and reporting bugs, you're helping us making our 
utilities more stable.

While we recommend against using `fases` in production systems, we do 
encourage testing and reporting.

Donations
---------
Donations really help this project, no matter how small. People that work 
on this project are unpaid volunteers that do it on their freetime.
We currently only accept XMR/Monero. You can use these addresses for 
donating.

Project Donations: 
`
83bXvP25H5SX3kfTXMcjdm816KFUapPdN2eUV1gAF3JTNcoqNtG5ZxvfRUF81bZMMWXcX5kES1h1jCJCNyYZmrHGVeQBGNK
`

![](static/FasesDonationsQR.png)

The server hosting the project also costs money. To keep the server up and 
running, you can donate to this address:

Server Donations: 
`
85q7XiHDpjYVq3cQ3uBSMN9JchP4mgpBGRc6SY9G6vyjF22QAugJxYAabLt9bQDvj3DcqkDjQKmduCNPVbzq7xVdTLmu2LF
`

![](static/ServerDonationsQR.png)

You can also donate to other contributors separately.

Thank you!
----------
I would like to thank a few people there, mainly:

- Runxi Yu, for making a mirror on their git server at git.andrewyu.org, and 
linking the #fases libera.chat channel to rx, for testing fases on
OpenBSD and for hosting the mailing list.

- Leah Rowe, for adding lots of improvements on her 
[git repo](https://notabug.org/vimuser/fases) and for testing fases on OpenBSD.
[Donate](https://www.patreon.com/libreleah).

- Ron Nazarov, for fixing a weird bug in `yes` and adding useful features in 
it.

- Other people I forgot to add there. If I forgot to add you there, do send 
a patch or just tell me.

Help is greatly appreciated, thanks!
