Report a bug

The `fases` project is still in very early development, and as such you may 
spot some bugs while testing.

Before reporting a bug, take a look at the mailing list's archives to see if 
someone hasn't already reported the same bug.

Please note that things like:

* missing options
* a utility isn't behaving like how you want it to behave

Aren't considered as bugs, but things like:

* non-POSIX behaviour on fully implemented POSIX utilities
* unstability
* segmentation faults
* etc...

Are considered as bugs, and you should report those.

While reporting your bug, be sure to:

* Tell us how to reproduce it
* Explain exactly what's going on
* Tell us which git revision you're running
* Tell us which utility's affected

Here's a template you can use:

	Title: [BUG] cat concatenates gibberish
	
	`cat`, r267.3ab9daf
	
	Summary
	-------
	Concatenating doesn't work. `cat` just returns gibberish.
	
	How to reproduce
	----------------
	1. Compile
	2. Run cat on a file
	3. See gibberish

Send your bug report to fases-general@andrewyu.org.

Alternatively, you may also report the bug on our [chat](/CHAT.html).

Thank you for contributing!
