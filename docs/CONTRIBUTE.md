Contributions

Want to contribute to `fases`? Great! Here are some recommendations to 
make sure your patch can be accepted.

Write useful commit names
-------------------------

This is important. Many people write commit names that aren't serious.
Here are examples of commit names you should **not** use:

* "jenius fix" ;
* "Update cat.c" ;
* "aaa" ;
* "replace horrible head with okay head".

There are many other bad commit names you shouldn't use.
Here are examples of commit names you **should** use:

* "core/wc: Error handling." ;
* "Makefile: Fix bugs in the test suite" ;
* "sh: Move error handling to parseCommand()".

Commit descriptions can also be added, but they're not needed:

* "sh: Add support for comments" ;

	Example:
	$ echo 'Hello World' # Hello World
	This is now possible.

* "docs/: Finally add a license"

	The license used is a slightly-modified version of the FreeBSD Documentation
	permissive License, compatible with the GNU Free Documentation License.

The coding style
----------------

See [the coding style](/CODING_STYLE.html)

Use `git send-email`
--------------------

`fases` is mirrored on lots of Git forges, including some that support 
"Pull Requests". The usual, prefered way of sending patches is to use 
`git send-email`. Please do prefix the repo you want to patch 
('[PATCH fases.git]').

See [git-send-email.io](https://git-send-email.io)

You may also push your changes to a public repo and request that these 
changes be pulled to the main repo.
