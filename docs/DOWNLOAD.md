Download

The `fases` project currently doesn't have releases. When we'll have 
releases, they'll be available in this page or in the Cgit frontend.

You can, however, download daily archives. One daily `tar.gz` archive 
is available [here](https://git.vitali64.duckdns.org/utils/fases.git/snapshot/fases-master.tar.gz).

Alternatively, you can also `clone` the source code with `git`.

	$ git clone https://git.vitali64.duckdns.org/git/utils/fases.git

Feel free to replace the URL with [any mirror you'd like to use](/MIRRORS.html).

There's also a [Free Software Directory page for `fases`](https://directory.fsf.org/wiki/Fases), although the 
download links are out-of-date.
