FAQ

Here are questions someone could ask. Read it! It could answer some of your 
questions.

> Why another coreutils? There's already the GNU coreutils, Busybox, 
> the OpenBSD coreutils, and so on.

Most coreutils are either non-POSIX or use non-portable functions not 
specified by POSIX.

In contrast, we separate POSIX and non-POSIX utilities, and on all 
utilities (including non-POSIX ones) we make sure we use ***only*** POSIX 
functions, regardless of if a non-POSIX function is already implemented 
in many C libraries.

Please though, please do not trash those other coreutils. The fact that 
they're not fully POSIX doesn't mean they're bad. In fact, some of the 
non-POSIX features might actually be useful for some people. Thanks!

> Is fases going to work on &lt;insert UNIX-like operating system name here&gt;?

Well, it depends on the system itself. If your system implements POSIX 
functions in its C library, then it'll work. Utilities are currently tested 
on:

* OpenBSD ;
* FreeBSD ;
* OS X/Mac OS X/MacOS (just a little) ;
* Artix Linux and Arch Linux ;
* Alpine Linux.

No. `fases` will never work natively on Microsoft Windows. Microsoft Windows 
doesn't implement any of the functions and features defined by POSIX. Maybe 
by using Cygwin or something similar it might work, but that's untested.

> Why is `fases` dormant? Is it abandonned?

Some people might see that `fases` goes dormant from time to time. The 
project isn't abandonned in any way! It's just that there only one 
maintainer and, as they're enjoying life or working on something else, 
they might not have time maintaining it. But keep in mind, ***`fases` 
is not dead!***

If you're interested in becoming a co-maintainer, please [reach out](/CHAT.html#Mailing lists)!
