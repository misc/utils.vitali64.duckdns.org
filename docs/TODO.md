Tasks

This is a TODO list, keeping track of things that should be 
worked on. If you want to contribute you can do so by picking 
one of those tasks and working on it!

Global:

* Test on lots of unix/unix-like operating systems
* Add lots of utils
* Add lots of options

chmod.c

* Support "a+rwx" and similar, as well as -R

date.c

* Be able to set the date
* Add `-u`

ln.c

* Support this synposis: `$ ln [-fs] [-L|-P] src target_dir`

ls.c

* Broken permissions

mkdir.c

* Implement -p

more.c

* Actually spend time and effort on making it good

printf.c

* Fix string formatting

rm.c

* Implement all options, rm -R doesn't recursively
remove directories

vi.c

* Doesn't compile

wc.c

* Make it be able to use standard output
