Discuss and send patches

The `fases` community mainly uses IRC:

`#fases` on `rx` ([webchat](https://gamja.irc.andrewyu.org/#fases)):

* server: `irc.andrewyu.org`;
* ssl: `6697` (recommended);
* plain: `6667`.

`#fases` on `Libera.Chat` ([webchat](https://web.libera.chat/#fases)):

* server: `irc.libera.chat`;
* ssl: `6697` (recommended);
* plain: `6667`.

Although less used, we also have an XMPP channel:

* room: `fases@conference.vitali64.duckdns.org`.

All those channels are bridged together so you can talk to us regardless of 
which chat platform you use.

Rules
-----
Here are chat rules for `#fases`:

* Don't ask to ask. You won't get banned right after asking to ask but 
  we prefer you ask your question right away, else less people will 
  want to answer your question. That also includes just saying "hi" and 
  waiting for someone to answer. Just ask your question, and someone 
  might answer. Keep in mind that we all are unpaid volunteers, and as 
  such it might take hours or even days for someone to answer.

* Don't spam. Spamming is a waste of time for chat operators and 
  an annoyance for other people in the chat. If you want to paste a text 
  with multiple lines (For example, output of a command) then use a 
  pastebin. Good sane pastebins include [https://paste.debian.net](https://paste.debian.net).

* Offtopic discussions are allowed, but please keep those at a minimum.

* English only. Moderating other languages is hard, because most people 
  in the chat are English speakers. 

* No discrimination. The chat rooms/channels are here for people to have 
  nice conversations with everyone. If that rule is broken, you *may* be 
  banned without warnings depending on severity.

* We use Libera.Chat for IRC chat. Libera.Chat has network policies all its 
  users are expected to follow, and `#fases` is no exception.
  Follow the [Libera.Chat network policies](https://libera.chat/policies/)!

If you break some of the rules, you'll be given *2* warnings before being 
banned in most cases. 

Operators/Moderators
--------------------
There's currently only one:

* Ferass El Hafidi, `f_` / `f_[xmpp]`.

If you think you can moderate then please let us know!

Mailing lists
-------------

The `fases` project also has a mailing list ran by Runxi Yu (thanks!)

The mailing list's address is: 
***fases-dev -at- andrewyu -dot- org*** 
([archives](https://archive.mail.andrewyu.org/fases-dev/)). 

To send patches, please use `git-send-email`. A tutorial on how to use 
it can be found [here](https://git-send-email.io/). You may also push your 
changes to a public repo and send a request to pull that, also via email.

[The rules above](#Rules) for the chat also apply to the mailing list.
