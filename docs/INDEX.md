Home

<div class="warn">
	<table><tbody><tr>
		<td><img src="../static/nuoveXT_warn.png"></td>
		<td><p><b>The <code>fases</code> project is looking for new maintainers!</b>
		The current maintainer unfourtunately does not have the time to work 
        on the project anymore. If you wish to take over maintenance, do chime in 
        the chat, thanks!</p></td>
	</tr></tbody></table>
</div>

The `fases` project tries to provide friendly, functional, simple, 
and compliant with the [POSIX.1-2017](https://pubs.opengroup.org/onlinepubs/9699919799/) specification (also known as 
IEEE Std 1003.1™-2017 or The Open Group Base Specifications Issue 7, 
2018 edition), core utilities for a fully functionnal UNIX-like Operating 
System. It tries to be entirely portable and working on any UNIX-like 
Operating System and kernel such as OpenBSD and Linux. It also tries 
to be completly modular and as such one utility should **not** depend 
on another in order to work. The coreutils are still a work-in-progress.
The `fases` utilities are currently tested on Artix, Alpine, OpenBSD, OS X and
FreeBSD. We expect all utilities to work on all systems implementing POSIX 
due to us using only POSIX-compliant functions.

`fases` is spelled with a lowercase `f` and is pronounced "phases". The name 
historically meant `Ferass' Base System` but the abbreviation soon 
became inapropriate because other people were contributing so it is now open 
to whatever abbreviation you prefer (`fine and simple experimental system`, 
`functionnal and simplistic expected system`, whatever...).

<div class="bigbuttons">
	<center><table><tbody><tr>
		<td><center><a href="DOWNLOAD.html"><img src="../static/nuoveXT_folder.png"><br/>Download <code>fases</code></a></center></td>
		<td><center><a href="CHAT.html"><img src="../static/nuoveXT_users.png"><br/>Communicate with us</a></center></td>
		<td><center><a href="BUGS.html"><img src="../static/nuoveXT_mailmessagenew.png"><br/>Report bugs</a></center></td>
		<td><center><a href="CONTRIBUTE.html"><img src="../static/nuoveXT_terminal.png"><br/>Hack on!</a></center></td>
	</tr></tbody></table></center>
</div>

Licensing
---------
The `fases` project is licensed under 2 separate licenses:

* The 3-Clause BSD license;
* The GNU GPL license, either version 3, or (if you want) any later version.

To take a look at which files use which license specifically, one may use the 
following command:

	$ git grep SPDX-License-Identifier | awk -F' ' '{ print $1 $3 }' | sed -e 's/:\/\*/: /g' -e 's/:#/: /g'

Some icons present on this website are taken from 
[LXDE's nuoveXT 2.2 icon theme](https://archlinux.org/packages/community/any/lxde-icon-theme/).

![](static/wip.gif)
