Build a fases+linux system

While the `fases` project isn't complete by any means, there has been some 
effort into making a minimal standalone operating system out of it, 
using `linux` as the kernel. This combination is called `fases+linux`.

Preparation
-----------
We need a root filesystem. First, create a work directory, like so:

	$ mkdir fasespluslinux
	$ cd fasespluslinux

Create the root filesystem:

	$ mkdir rootfs
	$ cd rootfs
	$ mkdir bin etc root
	$ cd ..

Now that we have a (incomplete) root filesystem, we need to compile 
`fases`.

You can compile `fases` separately or in a single binary; refer to 
[the compiling instructions](/COMPILE.html) for more info.
You do need to link statically. We recommend using the musl libc 
if on a GNU+Linux system or on an Alpine(-based) system.

After compiling `fases`, you can copy the binary/binaries to 
`rootfs/bin/`. For example, if you built `fases box`:

	$ cd fases
	<...>
	$ cd ..
	$ cp fases/box rootfs/bin/box

As the `fases` shell still isn't complete, it's encouraged to use 
a different shell implementation for the time being. If you want 
to use `dash`, for example:

	# These commands will only work on a GNU system!
	$ git clone https://git.kernel.org/pub/scm/utils/dash/dash
	$ cd dash
	$ ./autogen.sh
	$ ./configure CFLAGS=-static # Static linking.
	$ make

Copy the `dash` binary to `rootfs/bin/`.

	$ cd ..
	$ cp dash/src/dash rootfs/bin/

Go back to the rootfs:

	$ cd rootfs

Now we need to create an `etc/passwd` file. Create it and add the 
following:

	root::0:0::/root:/bin/dash

Replace `/bin/dash` with whatever shell you used.

Optionally create an `etc/os-release` file:

	NAME="fases+linux"
	PRETTY_NAME="fases plus linux"
	ID=fasespluslinux
	BUILD_ID=rolling
	ANSI_COLOR="';36"
	HOME_URL="https://utils.vitali64.duckdns.org"
	DOCUMENTATION_URL="https://utils.vitali64.duckdns.org"
	SUPPORT_URL="https://web.libera.chat/#fases"
	BUG_REPORT_URL="https://utils.vitali64.duckdns.org/BUGS.html"
	LOGO=fases

Feel free to modify this file to your liking.

Create the `init` file. Feel free to modify it to your liking:

	#!/bin/dash

	# If you compiled fases box and want everything symlinked.

	for u in basename cat chmod chown date dirname echo false head link ln ls \
	mkdir more mv printf rm sleep tail test true uname unlink
	do 
		box ln -s "/bin/box" "/bin/$u"
	done

	echo "Welcome to fases+linux!"
	exec dash

Replace `/bin/dash` with whatever shell you used.

Now clone the `linux` source tree. You can use any source tree of your 
choice, such as `linux-libre` or `linux-zen`.

	$ cd ..
	$ git clone https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git
	$ cd linux

Configure the kernel. Make at least the following selections:

	General Setup --->
		Default init path
			/init
		[*] Initial RAM filesystem and RAM disk (initramfs/initrd) support
			Initramfs source file(s)
				../rootfs/
	Executable file formats --->
		[*] Kernel support for ELF binaries
		[*] Kernel supports for scripts starting with #!
	Device Drivers --->
		Character devices --->
			[*] Enable TTY
	Generic Driver Options -->
		[*] Maintain a devtmpfs filesystem to mount at /dev
		[*]   Automount devtmpfs at /dev, after the kernel mounted the rootfs

Build
-----
That's a very trivial process. In the kernel source tree, run:

	$ make # -jX if you want multithreading.

And watch it compile!

Run
---
The image is located at `arch/$ARCH/boot/$IMAGE`. This is not a disk image, 
in that, you cannot boot it directly. You need to use a bootloader to load 
it. You can also use QEMU to boot it up, like so:

	$ qemu-system-x86_64 -kernel <image> ...

Have fun!
