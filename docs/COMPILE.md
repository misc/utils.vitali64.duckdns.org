Compile

You have 2 choices:

* compile each utility separately;

* compile only fasesiab which includes all utilities in a single binary.

Modify the `config.mk` file to fit your needs in both cases.

Compiling each utility separately
---------------------------------
For example to compile `cat`, you have to run the following:

	$ cd core/
	$ make cat

And to compile a non-POSIX utility such as `yes`, you have to run the 
following:

	$ cd extras/
	$ make yes

Compiling fases box
-------------------
`fases box` includes the entire collection of utilities `fases` provides 
in a single and tiny binary suitable for use in embedded systems.

Before compiling, you have to prepare `fases box`.
The `box.c` file should already contain some code but it may be outdated 
so it's recommended to regenerate it:

	$ make genbox

Next, run the following:

	$ make prepbox

Now you're ready to compile! Run `make` and get a binary!

	$ make box

Installation
------------
To install `fases box`, simply run the following *as root*:

	# make install-box

Or, if you want to install all utilities separately:

	# make install

Then, you should have a binary called `box`. Make sure `/usr/local/bin/` or 
whatever `DESTDIR` and `PREFIX` you set is in your `$PATH`.
