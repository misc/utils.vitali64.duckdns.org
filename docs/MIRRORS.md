Mirrors

Development of fases is done on git.vitali64.duckdns.org but there are 
mirrors made by other people:

Note that the mirrors are not always up-to-date with the current 
https://git.vitali64.duckdns.org upstream repo.

<table>
	<tr>
		<th>Protocol</th>
		<th>URL</th>
		<th>Author</th>
	</tr>
	<tr>
		<td>https,git</td>
		<td><a href="https://git.andrewyu.org/fun/fases.git">andrewyu.org</a></td>
		<td>Ferass El Hafidi and Runxi Yu</td>
	</tr>
	<tr>
		<td>https</td>
		<td><a href="https://notabug.org/vitali64/fases">notabug.org</a></td>
		<td>Ferass El Hafidi</td>
	</tr>
	<tr>
		<td>https</td>
		<td><a href="https://github.com/funderscore1/fases.git">github.com</a> (<strong>VERY OUT OF DATE</strong>)</td>
		<td>Ferass El Hafidi</td>
	</tr>
</table>

If you made a mirror too, be sure to let me know so I can add it on this 
section too!
